var one={
	Name:"Ryuji Sakamoto",
	CodeName:"Skull",
	Persona:"Captain Kidd",
	Arcana:"Arcana and allows the Phantom Thieves to defeat lower-level Shadows",
	imageUrl:"https://vignette.wikia.nocookie.net/megamitensei/images/b/bc/Ryuji_Sakamoto.png/revision/latest?cb=20160505182138",
	add:false,
	p:0
};
var two={
	Name:"Morgana",
	CodeName:"Mona",
	Persona:"Zorro",
	Arcana:"Morgana represents the Magician Arcana as a Confidant, allowing the protagonist to craft a variety of tools to help the Phantom Thieves explore the Metaverse",
	imageUrl:"https://vignette.wikia.nocookie.net/megamitensei/images/6/68/P5_Morgana_character_artwork.png/revision/latest?cb=20160505181742",
	add:false,
	p:0
};
var three={
	Name:"Ann Takamaki",
	CodeName:"Panther",
	Persona:"Carmen",
	Arcana:"She represents the Lovers Arcana as a Confidant, and helps the protagonist negotiate with the Shadows.",
	imageUrl:"https://vignette.wikia.nocookie.net/megamitensei/images/b/be/An_takamaki.png/revision/latest?cb=20170426203909",
	add:false,
	p:0
};
var four={
	Name:"Yusuke Kitagawa",
	CodeName:"Fox",
	Persona:"Goemon",
	Arcana:"As a Confidant, Yusuke represents the Emperor Arcana and assists the Phantom Thieves by copying skill cards.",
	imageUrl:"https://vignette.wikia.nocookie.net/megamitensei/images/5/5c/Yusuke-Kitagawa.png/revision/latest?cb=20160505181913",
	add:false,
	p:0
};
var five={
	Name:"Makoto Niijima",
	CodeName:"Queen",
	Persona:"Johanna ",
	Arcana:"As a Confidant, Makoto represents the Priestess Arcana and provides the Phantom Thieves with a more-detailed analysis of in-game enemies.",
	imageUrl:"https://vignette.wikia.nocookie.net/megamitensei/images/a/af/P5_Makoto_Nijima.png/revision/latest?cb=20160511211058",
	add:false,
	p:0
};

var person={one, two, three, four, five};

document.getElementById("k1").innerHTML =person.one.Name;
document.getElementById("k2").innerHTML =person.two.Name;
document.getElementById("k3").innerHTML =person.three.Name;
document.getElementById("k4").innerHTML =person.four.Name;
document.getElementById("k5").innerHTML =person.five.Name;

document.getElementById("k1").onclick = function() {add(person.one)};
document.getElementById("k2").onclick = function() {add(person.two)};
document.getElementById("k3").onclick = function() {add(person.three)};
document.getElementById("k4").onclick = function() {add(person.four)};
document.getElementById("k5").onclick = function() {add(person.five)};
var n=0;
function add(person) {
	
     if(n==0&&person.add==false){
	document.getElementById("c1").innerHTML = person.Name;
	person.add=true;
	person.p=1;
	n++;

     }
     if(n==1&&person.add==false){
	document.getElementById("c2").innerHTML = person.Name;
	person.add=true;
	person.p=2;
	n++;
	
     }
     if(n==2&&person.add==false){
	document.getElementById("c3").innerHTML = person.Name;
	person.add=true;
	person.p=3;
	n++;
	     	
     }
   
    document.getElementById("pro").innerHTML ="Name: "+person.Name+"<br><br>Code Name: "+person.CodeName+"<br><br>Persona: "+person.Persona+"<br><br>Arcana: "+person.Arcana+"<br><br><img src="+person.imageUrl+" alt=\"gg\" >";
     
}
document.getElementById("c1").onclick = function() {rm(1,person)};
document.getElementById("c2").onclick = function() {rm(2,person)};
document.getElementById("c3").onclick = function() {rm(3,person)};

function rm(p,person) {
	document.getElementById("c"+p).innerHTML = "";
	n--;
	if(p==person.one.p){
		person.one.p=0;
		person.one.add=false;
	};
	if(p==person.two.p){
		person.two.p=0;
		person.two.add=false;
	};
	if(p==person.three.p){
		person.three.p=0;
		person.three.add=false;
		
	};
	if(p==person.four.p){
		person.four.p=0;
		person.four.add=false;
		};
	if(p==person.five.p){
		person.five.p=0;
		person.five.add=false;
	};
		
	
}


